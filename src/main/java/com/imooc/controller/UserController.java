package com.imooc.controller;

import com.imooc.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping("/getNameByd")
    private String getNameById(int id){
        return userService.getNameById(id);
    }
}
