package com.imooc.dao;

import com.imooc.Entity.UserEntity;
import org.apache.ibatis.annotations.Param;

public interface UserDao {

    UserEntity findByd(@Param("id") int id);

    long insert(UserEntity userEntity);
}
