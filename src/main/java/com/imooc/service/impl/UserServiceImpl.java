package com.imooc.service.impl;

import com.imooc.Entity.UserEntity;
import com.imooc.dao.UserDao;
import com.imooc.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    public String getNameById(int id) {
        UserEntity userEntity = userDao.findByd(1);
        if (userEntity != null) {
            return userEntity.getName();
        }
        return null;
    }
}
