package com.imooc.Entity;

public class UserEntity {

    /**
     * 姓名
     */
    private String name = "rick";

    /**
     * 密码
     */
    private String password = "123321";

    public UserEntity() {
    }

    public UserEntity(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
